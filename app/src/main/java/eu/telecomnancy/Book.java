package eu.telecomnancy;


public class Book {

    private String Isbn;
    private String Name;
    private double Price;

    public String getIsbn() {
        return Isbn;
    }

    public void setIsbn(String isbn) {
        Isbn = isbn;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public double getPrice() {
        return Price;
    }

    public void setPrice(double price) {
        Price = price;
    }

    public Book(String isbn, String name, double price) {
        Isbn = isbn;
        Name = name;
        Price = price;
    }



}