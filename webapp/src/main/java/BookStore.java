import java.util.ArrayList;

import eu.telecomnancy.Book;

public class BookStore {

    private ArrayList<Book> database;

    public void addBook(Book b){
        
        
        for(Book bi:database){

            if (bi.getIsbn().equals(b.getIsbn())){
                return;
            }

        }
        
        this.database.add(b);

    }


    public void removeBook(String id){

        for(Book b:database){

            if (b.getIsbn().equals(id)){
                database.remove(b);
            }

        }
    }


    public void updateBook(String id, Book b){
        
        for(Book bi:this.database){
            
            if(bi.getIsbn().equals(id)){
                this.database.remove(bi);
                this.database.add(b);
            }

        }

    }

    public Book getBook(String id){
        
        for(Book b:this.database){
            
            if(b.getIsbn().equals(id)){
                return b;
            }

        }

        return null;
    }

    public BookStore() {

        this.database = new ArrayList<>();
        
    }


}